﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HeavyManager.UI.Startup))]
namespace HeavyManager.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
