﻿CREATE TABLE [dbo].[Projecto] (
    [IdProjecto] UNIQUEIDENTIFIER DEFAULT (newsequentialid()) NOT NULL,
    [Nome]       VARCHAR (50)     NOT NULL,
    [IsActive]   BIT              DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([IdProjecto] ASC)
);

