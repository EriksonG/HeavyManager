﻿CREATE TABLE Projecto
(
  IdProjecto UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWSEQUENTIALID(),
  Nome       VARCHAR(50) NOT NULL,
  IsActive   BIT         NOT NULL DEFAULT 1
)

INSERT INTO Projecto
CREATE TABLE EstadoTarefa
(
  IdTarefa INT         NOT NULL PRIMARY KEY,
  Estado   VARCHAR(25) NOT NULL
)
INSERT INTO EstadoTarefa VALUES (1, 'Criado')
INSERT INTO EstadoTarefa VALUES (2, 'Em Tratamento')
INSERT INTO EstadoTarefa VALUES (3, 'Em Pausa')
INSERT INTO EstadoTarefa VALUES (4, 'Terminado')
INSERT INTO EstadoTarefa VALUES (5, 'Cancelado')

CREATE TABLE Tarefa
(
  IdTarefa       INT           NOT NULL                                          IDENTITY PRIMARY KEY,
  Nome           VARCHAR(100)  NOT NULL,
  IdEstadoTarefa INT           NOT NULL REFERENCES EstadoTarefa (IdEstadoTarefa) DEFAULT 1,
  DataCriacao    DATETIME      NOT NULL                                          DEFAULT GETDATE(),
  IdUsuario      NVARCHAR(128) NOT NULL REFERENCES AspNetUsers (Id),
  IdTarefaPai    INT REFERENCES Tarefa (IdTarefa)
)

CREATE TABLE UsuarioTarefa
(
  IdTarefa  INT           NOT NULL REFERENCES Tarefa (IdTarefa),
  IdUsuario NVARCHAR(128) NOT NULL REFERENCES AspNetUsers (Id),
  PRIMARY KEY (IdTarefa, IdUsuario)
)

CREATE TABLE TarefaHist
(
  IdTarefa INT NOT NULL PRIMARY KEY REFERENCES Tarefa(IdTarefa),
  Data DATETIME NOT NULL DEFAULT GETDATE(),
  IdUsuario NVARCHAR(128) NOT NULL REFERENCES AspNetUsers (Id),
  Id
)