﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HeavyManager.UI.Controllers
{
    public class HomeController : HMController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}